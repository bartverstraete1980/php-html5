<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 2-jul-2015
 * Time: 15:08:05
 */
namespace phphtml5\Enums;

class Crossorigin extends Enum {

    const ANONYMOUS = 'anonymous',
            USECREDENTIALS = 'use-credentials';

}
