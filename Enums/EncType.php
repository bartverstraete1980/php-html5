<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 12-jun-2015
 * Time: 20:11:57
 */
namespace phphtml5\Enums;

class EncType extends Enum {
    const APPLICATION = 'application/x-www-form-urlencoded',
            MULTIPART = 'multipart/form-data',
            TEXT = 'text/plain';
}
