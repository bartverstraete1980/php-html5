<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 8-jul-2015
 * Time: 15:14:51
 */
namespace phphtml5\Enums;

class ButtonType extends Enum {
    const BUTTON = 'button',
            RESET = 'reset',
            SUBMIT = 'submit';
}
