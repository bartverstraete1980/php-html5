<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 2-jul-2015
 * Time: 12:36:57
 */
namespace phphtml5\Enums;

class MediaType extends Enum {

    const TEXTCSS = 'text/css',
            TEXTJAVASCRIPT = 'text/javascript',
            TEXTECMASCRIPT = 'text/ecmascript',
            APPLICATIONJAVASCRIPT = 'application/javascript',
            APPLICATIONECMASCRIPT = 'application/ecmascript',
            IMAGEXICON = 'image/x-icon';

}
