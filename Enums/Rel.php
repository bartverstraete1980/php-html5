<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 2-jul-2015
 * Time: 15:32:07
 */
namespace phphtml5\Enums;

class Rel extends Enum {

    const ALTERNATE = 'alternate',
            ARCHIVES = 'archives',
            AUTHOR = 'author',
            BOOKMARK = 'bookmark',
            EXTERNAL = 'external',
            FIRST = 'first',
            HELP = 'help',
            ICON = 'icon',
            LAST = 'last',
            LICENSE = 'license',
            NEXT = 'next',
            NOFOLLOW = 'nofollow',
            NOREFERRER = 'noreferrer',
            PINGBACK = 'pingback',
            PREFETCH = 'prefetch',
            PREV = 'prev',
            SEARCH = 'search',
            SIDEBAR = 'sidebar',
            STYLESHEET = 'stylesheet',
            TAG = 'tag',
            UP = 'up';

}
