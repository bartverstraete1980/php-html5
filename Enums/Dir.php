<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 12:15:13
 */
namespace phphtml5\Enums;

class Dir extends Enum {
    const RTL = 'rtl',
            LTR = 'ltr', 
            AUTO = 'auto';
}
