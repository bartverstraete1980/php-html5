<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 17-jun-2015
 * Time: 20:22:58
 */
namespace phphtml5\Enums;

class Type extends Enum {
    const  BUTTON = 'button',
        CHECKBOX = 'checkbox',
        COLOR = 'color',
        DATE = 'date',
        DATETIME = 'datetime',
        DATETIMELOCAL = 'datetime-local',
        EMAIL = 'email',
        FILE = 'file',
        HIDDEN = 'hidden',
        IMAGE = 'image',
        MONTH = 'month',
        NUMBER = 'number',
        PASSWORD = 'password',
        RADIO = 'radio',
        RANGE = 'range',
        RESET = 'reset',
        SEARCH = 'search',
        SUBMIT = 'submit',
        TEL = 'tel',
        TEXT = 'text',
        TIME = 'time',
        URL = 'url',
        WEEK = 'week';
}