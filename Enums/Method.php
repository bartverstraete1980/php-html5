<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jun-2015
 * Time: 22:02:57
 */
namespace phphtml5\Enums;

class Method extends Enum {
    const GET = 'GET',
            POST = 'POST';

}
