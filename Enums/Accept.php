<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 13-jun-2015
 * Time: 13:43:18
 */
namespace phphtml5\Enums;

class Accept extends Enum {
    const AUDIO = 'audio/*',
            VIDEO = 'video/*',
            IMAGE = 'image/*';
}
