<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 6-jun-2015
 * Time: 16:05:33
 */
namespace phphtml5\Enums;

class Declaration extends Enum {
    const 
        HTML5 = "html";
    
}
