<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 16:14:19
 */
namespace phphtml5\Enums;

class Dropzone extends Enum {
    const COPY = 'copy',
            MOVE = 'move',
            LINK = 'link';
}
