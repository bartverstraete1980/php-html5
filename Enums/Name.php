<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 10-jun-2015
 * Time: 19:38:34
 */
namespace phphtml5\Enums;

class Name extends Enum {
    const APPLICATIONNAME = 'application-name',
            AUTHOR = 'author',
            DESCRIPTION = 'description',
            GENERATOR = 'generator',
            KEYWORDS = 'keywords';
}
