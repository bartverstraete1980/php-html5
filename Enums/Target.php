<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 12-jun-2015
 * Time: 21:16:02
 */
namespace phphtml5\Enums;

class Target extends Enum {
    const _BLANK = '_blank',
            SELF = '_self',
            PARENT = '_parent',
            TOP = '_top';
}
