<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 10-jun-2015
 * Time: 14:12:27
 */
namespace phphtml5\Enums;

class Charset extends Enum {
    const UTF8 = 'UTF-8',
            ISO88591 = 'ISO-8859-1';
}
