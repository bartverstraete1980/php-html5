<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 12:07:56
 */
namespace phphtml5\Enums;

abstract class Enum {
    private static $constantsCache = [];
    private $value;
    
    const __default = null;


    public function __construct($value = null)
    {
        if (!self::has($value))
        {
            throw new \UnexpectedValueException("Value '$value' is not part of the enum " . get_called_class());
        }
 
        $this->value = $value;
    }
 
    public function is($value)
    {
        return $this->value === $value;
    }
 
    public function value()
    {
        return $this->value;
    }
 
    public static function has($value)
    {
        return in_array($value, self::toArray(), true);
    }
 
    public static function toArray()
    {
        $calledClass = get_called_class();
 
        if(!array_key_exists($calledClass, self::$constantsCache))
        {
            $reflection = new \ReflectionClass($calledClass);
            self::$constantsCache[$calledClass] = $reflection->getConstants();
        }
 
        return self::$constantsCache[$calledClass];
    }

    public function __toString() {
        return (string)$this->value;
    }

}