<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 20-jun-2015
 * Time: 13:26:14
 */
namespace phphtml5\Enums;

class Wrap extends Enum {
    const HARD = 'hard',
            SOFT = 'softr';
}
