<?php
namespace phphtml5\jQueryUI;

use Exception;
use phphtml5\Tags\FormTag;
use phphtml5\Tags\OptionTag;
use phphtml5\Tags\SelectTag;
use phphtml5\Tags\Tag;
use function exceptionHandler;

/**
 * Description of LabelSelect
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class LabelSelect extends \phphtml5\Tags\LabelTag {
    public function __construct(Tag &$parent, $id, $label = NULL,
            array $data = NULL, $selected = NULL, $name = NULL) {
        parent::__construct($parent, NULL, $parent instanceof FormTag ? $parent->getId() : NULL);
        try {
            $this->setId("LabelSelect_$id");
            if (!is_null($label) && is_string($label)) {
                $this->appendElement($label);
            }
            $this->appendElement($s = $this->getSelect($this, $id, $data,
                    $selected, $parent instanceof FormTag ? $parent->getId() :
                    $name));
        } catch (Exception $exc) {
            exceptionHandler($exc);
        }
    }
    private function getSelect(Tag &$parent, $id, array $data = NULL,
            $selected = NULL, $form = NULL, $name = NULL) {
        $s = new SelectTag($parent, NULL, NULL, $form, NULL, $name,
                NULL, NULL);
        $s->setId($id);
        foreach ($data as $key => $value) {
            $s->appendElement($o = new OptionTag($s, NULL, NULL, $selected == $key ?
                    TRUE : FALSE, $key));
            $o->appendElement($key);
        }
        return $s;
    }
}
