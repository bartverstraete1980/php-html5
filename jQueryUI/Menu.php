<?php
namespace phphtml5\jQueryUI;

use Exception;
use phphtml5\Enums\MediaType;
use phphtml5\Tags\ATag;
use phphtml5\Tags\DivTag;
use phphtml5\Tags\LiTag;
use phphtml5\Tags\ScriptTag;
use phphtml5\Tags\Tag;
use phphtml5\Tags\UlTag;
use function exceptionHandler;

/**
 * Description of Menu
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Menu extends DivTag {
    private $ul;
    private $script;

    public function __construct(Tag &$parent, $id, $data) {
        parent::__construct($parent);
        try {
            $this->ul = new UlTag($this);
            $this->ul->setId($id);
            
            $this->fill($data, $this->ul);
            
            $this->appendElement($this->ul);
//            $this->script = new ScriptTag(NULL, NULL, NULL, NULL,
//                    new MediaType(MediaType::TEXTJAVASCRIPT));
//            $this->script->appendElement("$(function(){\$('#$id').menu();});");
//            $this->appendElement($this->script);
        } catch (Exception $exc) {
            exceptionHandler($exc);
        }
    }
    private function fill($data, UlTag $ul) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_string($value)) {
                    $li = new LiTag($ul);
                    $a = new ATag($li, NULL, $value, NULL, NULL, NULL, NULL,
                            NULL);
                    $a->appendElement($key);
                    $li->appendElement($a);
                    $ul->appendElement($li);
                } elseif (is_array($value)) {
                    $li = new LiTag($ul);
                    $a = new ATag($li, NULL, '#', NULL, NULL, NULL, NULL, NULL);
                    $a->appendElement($key);
                    $nuwul = new UlTag($li);
                    $this->fill($value, $nuwul);
                    $li->appendElement($a);
                    $li->appendElement($nuwul);
                    $ul->appendElement($li);
//                    $this->fill($value, $ul);
                }
            }
        }
    }
}
