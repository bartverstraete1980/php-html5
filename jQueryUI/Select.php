<?php
namespace phphtml5\jQueryUI;

use Exception;
use phphtml5\Tags\OptionTag;
use phphtml5\Tags\SelectTag;
use phphtml5\Tags\Tag;

/**
 * Description of newPHPClass
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Select extends SelectTag {
    private $selected;
    
    public function __construct(Tag &$parent, $name, array $data = NULL,
            $selected = NULL, $form_id = NULL) {
        parent::__construct($parent, NULL, NULL, $form_id, NULL, "{$name}_Select", NULL,
                NULL);
        try {
            $this->setId("{$name}_Select");
            $this->selected = $selected;
            foreach ($data as $key => $value) {
                $this->appendElement($o = $this->setOption($this, $key, $value));
            }
        } catch (Exception $exc) {
            exceptionHandler($exc);
        }
    }
    private function setOption(&$parent, $key, $value) {
        $o = new OptionTag($parent, NULL, $key, $key == $this->selected ? TRUE : FALSE, $value);
        $o->appendElement($key);
        return $o;
    }
}
