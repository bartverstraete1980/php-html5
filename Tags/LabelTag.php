<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 12-jun-2015
 * Time: 22:08:23
 */
namespace phphtml5\Tags;

class LabelTag extends TagWithGlobalAttributes {
    private $for, $form; //Attributes
    
    function __construct(Tag &$parent = null, $for = null, $form = null) {
        parent::__construct('label', $parent);
        try {
            $this->setFor($for);
            $this->setForm($form);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->for)) {
            $this->appendAttribute('for', $this->for);
        }
        if (isset($this->form)) {
            $this->appendAttribute('form', $this->form);
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return string
     */
    function getFor() {
        return $this->for;
    }

    /**
     * 
     * @return string
     */
    function getForm() {
        return $this->form;
    }

    /**
     * 
     * @param string $for
     * @throws Exception
     */
    function setFor($for) {
        if (is_null($for)) {
            $this->for = null;
        } elseif (is_string($for)) {
            $this->for = $for;
        } else {
            throw new Exception('$for isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param string $form
     * @throws Exception
     */
    function setForm($form) {
        if (is_null($form)) {
            $this->form = null;
        } elseif (is_string($form)) {
            $this->form = $form;
        } else {
            throw new Exception('$form isn\'t a valid string!');
        }
    }
}