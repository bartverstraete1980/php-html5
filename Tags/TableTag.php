<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 23:02:38
 */
namespace phphtml5\Tags;

class TableTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('table');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

}
