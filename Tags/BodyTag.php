<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 10-jun-2015
 * Time: 19:02:28
 */
namespace phphtml5\Tags;

class BodyTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('body');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}