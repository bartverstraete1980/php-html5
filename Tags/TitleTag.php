<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 17:43:19
 */
namespace phphtml5\Tags;

class TitleTag extends TagWithGlobalAttributes {
    private $title;
    
    function __construct() {
        parent::__construct('title');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}
