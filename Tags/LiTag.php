<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 16:41:26
 */
namespace phphtml5\Tags;

class LiTag extends TagWithGlobalAttributes {

    private $value; //Attributes

    function __construct(Tag &$parent = null,$value = null) {
        parent::__construct('li', $parent);
        try {
            $this->setValue($value);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->value)) {
            $this->appendAttribute('vamue', $this->value);
        }
        return parent::sAttributes();
    }

    function getValue() {
        return $this->value;
    }

    function setValue($value) {
        if (is_null($value)) {
            $this->value = null;
        } elseif (is_int($value)) {
            $this->value = $value;
        } else {
            throw new Exception('$value isn\'t a valid int!');
        }
    }

}
