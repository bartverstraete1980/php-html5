<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 16:35:09
 */
namespace phphtml5\Tags;

class UlTag extends TagWithGlobalAttributes {

    function __construct(Tag &$parent = null) {
        parent::__construct('ul',$parent);
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
        }
    
    public function appendElement($element) {
        if ($element instanceof LiTag) {
            return parent::appendElement($element);
        }
    }
}