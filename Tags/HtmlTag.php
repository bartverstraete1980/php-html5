<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 6-jun-2015
 * Time: 14:55:29
 */
namespace phphtml5\Tags;

class HtmlTag extends TagWithGlobalAttributes {
    private $manifest, $xmlns; //Attributes
            
    function __construct() {
        parent::__construct('html');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
        }

    protected function sAttributes() {
        if ($this->manifest) {
            $this->appendAttribute('manifest', $this->manifest);
        }
        if ($this->xmlns) {
            $this->appendAttribute(('xmlns'), $this->xmlns);
        }
        return parent::sAttributes();
    }

    function getManifest() {
        return $this->manifest;
    }

    function getXmlns() {
        return $this->xmlns;
    }

    function setManifest($manifest) {
        $this->manifest = $manifest;
    }

    function setXmlns($xmlns) {
        $this->xmlns = $xmlns;
    }

}