<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 17:41:30
 */
namespace phphtml5\Tags;

class PTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('p');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}