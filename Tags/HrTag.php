<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jun-2015
 * Time: 21:06:45
 */
namespace phphtml5\Tags;

class HrTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('hr');
        try {
            $this->closetag = true;
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}