<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jun-2015
 * Time: 16:46:43
 */
namespace phphtml5\Tags;

class HxTag extends TagWithGlobalAttributes {

    function __construct($x) {
        parent::__construct("h$x");
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}