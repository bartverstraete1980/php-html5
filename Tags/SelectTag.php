<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 13-jun-2015
 * Time: 17:33:37
 */
namespace phphtml5\Tags;

class SelectTag extends TagWithGlobalAttributes {
    private $autofocus, $disabled, $form, $multiple, $name, $required, $size; //Attributes

    function __construct(Tag &$parent, $autofocus = null, $disabled = null,
            $form = null, $multiple = null, $name = null, $required = null,
            $size = null) { 
        parent::__construct('select', $parent);
        try {
            $this->setAutofocus($autofocus);
            $this->setDisabled($disabled);
            $this->setForm($form);
            $this->setMultiple($multiple);
            $this->setName($name);
            $this->setRequired($required);
            $this->setSize($size);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if(isset($this->autofocus) && $this->autofocus) {
            $this->appendAttribute('autofocus');
        }
        if(isset($this->disabled) && $this->disabled) {
            $this->appendAttribute('disabled');
        }
        if(isset($this->form)) {
            $this->appendAttribute('form', $this->form);
        }
        if(isset($this->multiple) && $this->multiple) {
            $this->appendAttribute('multiple');
        }
        if(isset($this->name)) {
            $this->appendAttribute('name', $this->name);
        }
        if(isset($this->required) && $this->required) {
            $this->appendAttribute('required');
        }
        if(isset($this->size)) {
            $this->appendAttribute('size', $this->size);
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return bool
     */
    function getAutofocus() {
        return $this->autofocus;
    }

    /**
     * 
     * @return bool
     */
    function getDisabled() {
        return $this->disabled;
    }

    /**
     * 
     * @return string
     */
    function getForm() {
        return $this->form;
    }

    /**
     * 
     * @return bool
     */
    function getMultiple() {
        return $this->multiple;
    }

    /**
     * 
     * @return string
     */
    function getName() {
        return $this->name;
    }

    /**
     * 
     * @return bool
     */
    function getRequired() {
        return $this->required;
    }

    /**
     * 
     * @return int
     */
    function getSize() {
        return $this->size;
    }

    /**
     * 
     * @param bool $autofocus
     * @throws Exception
     */
    function setAutofocus($autofocus) {
        if (is_null($autofocus)) {
            $this->autofocus = null;
        } elseif (is_bool($autofocus)) {
            $this->autofocus = $autofocus;
        } else {
            throw new Exception('$autofocus isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $disabled
     * @throws Exception
     */
    function setDisabled($disabled) {
        if (is_null($disabled)) {
            $this->disabled = null;
        } elseif (is_bool($disabled)) {
            $this->disabled = $disabled;
        } else {
            throw new Exception('$disabled isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $form
     * @throws Exception
     */
    function setForm($form) {
        if (is_null($form)) {
            $this->form = null;
        } elseif (is_string($form)) {
            $this->form = $form;
        } else {
            throw new Exception('$form isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param bool $multiple
     * @throws Exception
     */
    function setMultiple($multiple) {
        if (is_null($multiple)) {
            $this->multiple = null;
        } elseif (is_bool($multiple)) {
            $this->multiple = $multiple;
        } else {
            throw new Exception('$multiple isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $name
     * @throws Exception
     */
    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif (is_string($name)) {
            $this->name = $name;
        } else {
            throw new Exception('$name isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param bool $required
     * @throws Exception
     */
    function setRequired($required) {
        if (is_null($required)) {
            $this->required = null;
        } elseif (is_bool($required)) {
            $this->required = $required;
        } else {
            throw new Exception('$required isn\t a valid bool!');
        }
    }

    /**
     * 
     * @param int $size
     * @throws Exception
     */
    function setSize($size) {
        if (is_null($size)) {
            $this->size = null;
        } elseif (is_int($size)) {
            $this->size = $size;
        } else {
            throw new Exception("$size isn\'t a valid int!");
        }
    }
}