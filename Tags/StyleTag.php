<?php
namespace phphtml5\Tags;

/**
 * Description of StyleTag
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class StyleTag extends TagWithGlobalAttributes {
    public function __construct() {
        parent::__construct('style');
    }
}
