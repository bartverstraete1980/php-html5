<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 2-jul-2015
 * Time: 18:47:10
 */
namespace phphtml5\Tags;

class ATag extends TagWithGlobalAttributes {

    private $download, $href, $hreflang, $media, $rel, $target, $type; //Attributes

    function __construct(Tag &$parent = null, $download = null, $href = null, \phphtml5\Enums\Language $hreflang = null, $media = null, \phphtml5\Enums\Rel $rel = null, \phphtml5\Enums\Target $target = null, \phphtml5\Enums\MediaType $type = null) {
        parent::__construct('a', $parent);
        try {
            $this->setDownload($download);
            $this->setHref($href);
            $this->setHreflang($hreflang);
            $this->setMedia($media);
            $this->setRel($rel);
            $this->setTarget($target);
            $this->setType($type);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if(isset($this->download)) {
            if (is_bool($this->download) && $this->download == true) {
                $this->appendAttribute('download');
            } else {
                //$download is a string
                $this->appendAttribute('download', $this->download);
            }
        }
        if(isset($this->href)) {
            $this->appendAttribute('href', $this->href);
        }
        if(isset($this->hreflang) && $this->hreflang->value() != null) {
            $this->appendAttribute('hreflang', $this->hreflang->value());
        }
        if(isset($this->media)) {
            $this->appendAttribute('media', $this->media);
        }
        if(isset($this->rel) && $this->rel->value() != null) {
            $this->appendAttribute('rel', $this->rel->value());
        }
        if(isset($this->target) && $this->target->value() != null) {
            $this->appendAttribute('target', $this->target->value());
        }
        if(isset($this->type) && $this->type->value() != null) {
            $this->appendAttribute('type', $this->type->value());
        }
        return parent::sAttributes();
    }

    function getDownload() {
        return $this->download;
    }

    function getHref() {
        return $this->href;
    }

    function getHreflang() {
        return $this->hreflang;
    }

    function getMedia() {
        return $this->media;
    }

    function getRel() {
        return $this->rel;
    }

    function getTarget() {
        return $this->target;
    }

    function getType() {
        return $this->type;
    }

    function setDownload($download) {
        if (is_null($download)) {
            $this->download = null;
        } elseif (is_bool($download) || is_string($download)) {
            $this->download = $download;
        } else {
            throw new Exception('$download isn\'t a valid bool|string!');
        }
    }

    function setHref($href) {
        if (is_null($href)) {
            $this->href = null;
        } elseif (is_string($href)) {
            $this->href = $href;
        } else {
            throw new Exception('$href isn\'t a valid string!');
        }
    }

    function setHreflang(\phphtml5\Enums\Language $hreflang = null) {
        if (is_null($hreflang)) {
            $this->hreflang = new \phphtml5\Enums\Language();
        } else {
            $this->hreflang = $hreflang;
        }
    }

    function setMedia($media) {
        if (is_null($media)) {
            $this->media = null;
        } elseif (is_string($media)) {
            $this->media = $media;
        } else {
            throw new Exception('$media isn\t a valid string!');
        }
    }

    function setRel(\phphtml5\Enums\Rel $rel = null) {
        if (is_null($rel)) {
            $this->rel = new \phphtml5\Enums\Rel();
        } else {
            $this->rel = $rel;
        }
    }

    function setTarget(phphtml5\Enums\Target $target = null) {
        if (is_null($target)) {
            $this->target = new \phphtml5\Enums\Target();
        } else {
            $this->target = $target;
        }
    }

    function setType(\phphtml5\Enums\MediaType $type = null) {
        if (is_null($type)) {
            $this->type = new \phphtml5\Enums\MediaType();
        } else {
            $this->type = $type;
        }
    }
}