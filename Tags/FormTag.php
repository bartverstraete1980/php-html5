<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jun-2015
 * Time: 21:17:09
 */
namespace phphtml5\Tags;

class FormTag extends TagWithGlobalAttributes {
    private $acceptCharset, $action, $autocomplete, $enctype, $method, $name,
            $novalidate,$target; //Form attributes

    function __construct(\phphtml5\Enums\Charset $acceptCharset = null, $action = null,
            $autocomplete = null, \phphtml5\Enums\EncType $enctype = null, \phphtml5\Enums\Method $method = null,
            $name = null, $novalidate = null, \phphtml5\Enums\Target $target = null, Tag &$parent = NULL) {
        parent::__construct('form', $parent);
        try {
            $this->setAcceptCharset(is_null($acceptCharset) ? new \phphtml5\Enums\Charset() : $acceptCharset);
            $this->setAction($action);
            $this->setAutocomplete($autocomplete);
            $this->setEnctype(is_null($enctype) ? new \phphtml5\Enums\EncType() : $enctype);
            $this->setMethod(is_null($method) ? new \phphtml5\Enums\Method() : $method);
            $this->setName($name);
            $this->setNovalidate($novalidate);
            $this->setTarget(is_null($target) ? new \phphtml5\Enums\Target() : $target);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->acceptCharset) && !is_null($this->acceptCharset->value())) {
            $this->appendAttribute('accept-charset', $this->acceptCharset->value());
        }
        if (isset($this->action)) {
            $this->appendAttribute('action', $this->action);
        }
        if (isset($this->autocomplete)) {
            $this->appendAttribute('autocomplete', $this->autocomplete ? 'on' : 'off');
        }
        if (isset($this->enctype) && !is_null($this->enctype->value())) {
            $this->appendAttribute('enctype', $this->enctype->value());
        }
        if (isset($this->method) &&  !is_null($this->method->value())) {
            $this->appendAttribute('method', $this->method->value());
        }
        if (isset($this->name)) {
            $this->appendAttribute('name', $this->name);
        }
        if (isset($this->novalidate) && $this->name == true) {
            $this->appendAttribute('novalidate');
        }
        if (isset($this->target) && !is_null($this->target->value())) {
            $this->appendAttribute('target', $this->target->value());
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return \phphtml5\Enums\Charset
     */
    function getAcceptCharset() {
        return $this->acceptCharset;
    }

    /**
     * 
     * @return string
     */
    function getAction() {
        return $this->action;
    }

    /**
     * 
     * @return bool
     */
    function getAutocomplete() {
        return $this->autocomplete;
    }

    /**
     * 
     * @return \phphtml5\Enums\EncType
     */
    function getEnctype() {
        return $this->enctype;
    }

    /**
     * 
     * @return \phphtml5\Enums\Method
     */
    function getMethod() {
        return $this->method;
    }

    /**
     * 
     * @return string
     */
    function getName() {
        return $this->name;
    }

    /**
     * 
     * @return bool
     */
    function getNovalidate() {
        return $this->novalidate;
    }

    /**
     * 
     * @return \phphtml5\Enums\Target
     */
    function getTarget() {
        return $this->target;
    }

    /**
     * 
     * @param \phphtml5\Enums\Charset $acceptCharset
     */
    function setAcceptCharset(\phphtml5\Enums\Charset $acceptCharset) {
        $this->acceptCharset = $acceptCharset;
    }

    /**
     * 
     * @param string $action
     */
    function setAction($action) {
        if (is_null($action) || $action == '') {
            $this->action = null;
        } elseif (is_string($action)) {
            $this->action = $action;
        }  else {
            throw new Exception('$action isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param bool $autocomplete
     */
    function setAutocomplete($autocomplete) {
        if (is_null($autocomplete)) {
            $this->autocomplete = null;
        } elseif (is_bool($autocomplete)) {
            $this->autocomplete = $autocomplete;
        } else {
            throw new Exception('$autocomplete isn\'t a bool!');
        }
    }

    /**
     * 
     * @param \phphtml5\Enums\EncType $enctype
     */
    function setEnctype(\phphtml5\Enums\EncType $enctype) {
        $this->enctype = $enctype;
    }

    /**
     * 
     * @param \phphtml5\Enums\Method $method
     */
    function setMethod(\phphtml5\Enums\Method $method) {
        $this->method = $method;
    }

    /**
     * 
     * @param string $name
     * @throws Exception
     */
    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif (is_string($name)) {
            $this->name = $name;
        } else {
            throw new Exception('$name isn\t a valid string!');
        }
    }

    function setNovalidate($novalidate) {
        if (is_null($novalidate)) {
            $this->novalidate = null;
        } elseif (is_bool($novalidate)) {
            $this->novalidate = $novalidate;
        } else {
            throw new Exception('$novalidate isn\'t a bool!');
        }
    }

    /**
     * 
     * @param \phphtml5\Enums\Target $target
     */
    function setTarget(\phphtml5\Enums\Target $target) {
        $this->target = $target;
    }
}