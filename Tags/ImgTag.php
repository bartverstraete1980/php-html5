<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jul-2015
 * Time: 11:03:02
 */
namespace phphtml5\Tags;

class ImgTag extends TagWithGlobalAttributes {

    private $alt, $crossorigin, $height, $ismap, $longdesc, $src, $usemap, $width;

    function __construct($alt = null, \phphtml5\Enums\Crossorigin $crossorigin = null, $height = null, $ismap = null, $longdesc = null, $src = null, $usemap = null, $width = null) {
        parent::__construct('img');
        $this->closetag = true;
        try {
            $this->setAlt($alt);
            $this->setCrossorigin($crossorigin);
            $this->setHeight($height);
            $this->setIsmap($ismap);
            $this->setLongdesc($longdesc);
            $this->setSrc($src);
            $this->setUsemap($usemap);
            $this->setWidth($width);
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->alt)) {
            $this->appendAttribute('alt', $this->alt);
        }
        if (isset($this->crossorigin) && $this->crossorigin->value() != null) {
            $this->appendAttribute('crossorigin', $this->crossorigin->value());
        }
        if (isset($this->height)) {
            $this->appendAttribute('height', (string)$this->height);
        }
        if (isset($this->ismap) && $this->ismap == true) {
            $this->appendAttribute('ismap');
        }
        if (isset($this->longdesc)) {
            $this->appendAttribute('longdesc', $this->longdesc);
        }
        if (isset($this->src)) {
            $this->appendAttribute('src', $this->src);
        }
        if (isset($this->usemap)) {
            $this->appendAttribute('usemap', $this->usemap);
        }
        if (isset($this->width)) {
            $this->appendAttribute('width', (string)$this->width);
        }
        return parent::sAttributes();
    }

    function getAlt() {
        return $this->alt;
    }
    
    function getCrossorigin() {
        return $this->crossorigin;
    }

    function getHeight() {
        return $this->height;
    }

    function getIsmap() {
        return $this->ismap;
    }

    function getLongdesc() {
        return $this->longdesc;
    }

    function getSrc() {
        return $this->src;
    }

    function getUsemap() {
        return $this->usemap;
    }

    function getWidth() {
        return $this->width;
    }

    function setAlt($alt) {
        if (is_null($alt)) {
            $this->alt = null;
        } elseif (is_string($alt)) {
            if ($alt == '') {
                throw new Exception('$alt == \'\', a empty string is not allowed');
            } else {
                $this->alt = $alt;
            }
        } else {
           throw new Exception('$altisn\'t a valid string!'); 
        }
    }

    function setCrossorigin(\phphtml5\Enums\Crossorigin $crossorigin = null) {
        if (is_null($crossorigin)) {
            $this->crossorigin = new \phphtml5\Enums\Crossorigin();
        } else {
            $this->crossorigin = $crossorigin;
        }
    }

    function setHeight($height) {
        if (is_null($height)) {
            $this->height =null;
        } elseif (is_int($height)) {
            $this->height = $height;
        } else {
            throw new Exception('$height isn\'t a valid int!');
        }
    }

    function setIsmap($ismap) {
        if (is_null($ismap)) {
            $this->ismap = null;
        } elseif (is_bool($ismap)) {
            $this->ismap = $ismap;
        } else {
            throw new Exception('$ismap isn\'t a valid bool!');
        }
    }

    function setLongdesc($longdesc) {
        if (is_null($longdesc)) {
            $this->longdesc = null;
        } elseif (is_string($longdesc)) {
            $this->longdesc = $longdesc;
        } else {
            throw new Exception('$longdesc isn\'t a valid string!');
        }
    }

    function setSrc($src) {
        if (is_null($src)) {
            $this->src = null;
        } elseif (is_string($src)) {
            $this->src = $src;
        } else {
            throw new Exception('$src isn\'t a valid string!');
        }
    }

    function setUsemap($usemap) {
        if (is_null($usemap)) {
            $this->usemap = null;
        } elseif (is_string($usemap)) {
            if (!preg_match('/^#.+/i', $usemap)) {
                throw new Exception('$usemap has to be in the form of \'#name\'');
            }
            $this->usemap = $usemap;
        } else {
            throw new Exception('$usemap isn\'t a valid string!');
        }
    }

    function setWidth($width) {
        if (is_null($width)) {
            $this->width = null;
        } elseif (is_int($width)) {
            $this->width = $width;
        } else {
            throw new Exception('$width isn\'t a valid int!');
        }
    }
}