<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 16:55:18
 */
namespace phphtml5\Tags;

class HeadTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('head');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    public function appendElement($element) {
        switch (true) {
            case $element instanceof TitleTag:
            case $element instanceof StyleTag:
//            case $element instanceof BaseTag:
            case $element instanceof LinkTag:
            case $element instanceof MetaTag:
            case $element instanceof ScriptTag:
//            case $element instanceof NoScriptTag:
            case is_null($element):
                parent::appendElement($element);
                break;
            default :
                throw new Exception('$element is not a valid Tag!');
        }
    }
}