<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 13-jun-2015
 * Time: 13:25:37
 */
namespace phphtml5\Tags;
class InputTag extends TagWithGlobalAttributes {
    private $accept, $alt, $autocomplete, $autofocus, $checked, $disabled,/*6*/
            $form, $formaction, $formenctype, $formmethod, $formnovalidate,/*5*/
            $formtarget, $height, $list, $max, $maxlength, $min, $multiple,/*7*/
            $name, $pattern, $placeholder, $readonly, $required, $size, $src,/*7*/
            $step, $type, $value, $width; //Attributes /*4*/
    
    function __construct(\phphtml5\Enums\Accept $accept = null, $alt = null, $autocomplete = null,
            $autofocus = null, $checked = null, $disabled = null, $form = null,
            $formaction = null, \phphtml5\Enums\EncType $formenctype = null,
            \phphtml5\Enums\Method $formmethod = null, $formnovalidate = null,
            \phphtml5\Enums\Target $formtarget = null, $height = null, $list = null, $max = null,
            $maxlength = null, $min = null, $multiple = null, $name = null,
            $pattern = null, $placeholder = null, $readonly = null,
            $required = null, $size = null, $src = null, $step = null,
            \phphtml5\Enums\Type $type = null, $value = null, $width = null) {
        parent::__construct('input');
        $this->closetag = true;
        try {
            $this->setAccept($accept);
            $this->setAlt($alt);
            $this->setAutocomplete($autocomplete);
            $this->setAutofocus($autofocus);
            $this->setChecked($checked);
            $this->setDisabled($disabled);
            $this->setForm($form);
            $this->setFormaction($formaction);
            $this->setFormenctype($formenctype);
            $this->setFormmethod($formmethod);
            $this->setFormnovalidate($formnovalidate);
            $this->setFormtarget($formtarget);
            $this->setHeight($height);
            $this->setList($list);
            $this->setMax($max);
            $this->setMaxlength($maxlength);
            $this->setMin($min);
            $this->setMultiple($multiple);
            $this->setName($name);
            $this->setPattern($pattern);
            $this->setPlaceholder($placeholder);
            $this->setReadonly($readonly);
            $this->setRequired($required);
            $this->setSize($size);
            $this->setSrc($src);
            $this->setStep($step);
            $this->setType($type);
            $this->setValue($value);
            $this->setWidth($width);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if(isset($this->accept) && !is_null($this->accept->value())) {
            $this->appendAttribute('accept', $this->accept->value());
        }
        if(isset($this->alt)) {
            $this->appendAttribute('alt', $this->alt);
        }
        if(isset($this->autocomplete)) {
            $this->appendAttribute('autocomplete', $this->autocomplete ? 'on' : 'off');
        }
        if(isset($this->autofocus) && $this->autofocus) {
            $this->appendAttribute('autofocus');
        }
        if(isset($this->checked) && $this->checked) {
            $this->appendAttribute('checked');
        }
        if(isset($this->disabled) && $this->disabled) {
            $this->appendAttribute('disabled');
        }
        if(isset($this->form)) {
            $this->appendAttribute('form', $this->form);
        }
        if(isset($this->formaction)) {
            $this->appendAttribute('formaction', $this->formaction);
        }
        if(isset($this->formenctype) && !is_null($this->formenctype->value())) {
            $this->appendAttribute('formenctype', $this->formenctype->value());
        }
        if(isset($this->formmethod) && is_null($this->formmethod->value())) {
            $this->appendAttribute('formmethod', $this->formmethod->value());
        }
        if(isset($this->formnovalidate) && $this->formnovalidate) {
            $this->appendAttribute('formnovalidate');
        }
        if(isset($this->formtarget) && !is_null($this->formtarget->value())) {
            $this->appendAttribute('formtarget', $this->formtarget->value());
        }
        if(isset($this->height)) {
            $this->appendAttribute('height', $this->height);
        }
        if(isset($this->list)) {
            $this->appendAttribute('list', $this->list);
        }
        if(isset($this->max)) {
            $this->appendAttribute('max', $this->max);
        }
        if(isset($this->maxlength)) {
            $this->appendAttribute('maxlength', $this->maxlength);
        }
        if(isset($this->min)) {
            $this->appendAttribute('min', $this->min);
        }
        if(isset($this->multiple) && $this->multiple) {
            $this->appendAttribute('multiple');
        }
        if(isset($this->name)) {
            $this->appendAttribute('name', $this->name);
        }
        if(isset($this->pattern)) {
            $this->appendAttribute('pattern', $this->pattern);
        }
        if(isset($this->placeholder)) {
            $this->appendAttribute('placeholder', $this->placeholder);
        }
        if(isset($this->readonly) && $this->readonly) {
            $this->appendAttribute('readonly');
        }
        if(isset($this->required) && $this->required) {
            $this->appendAttribute('required');
        }
        if(isset($this->size)) {
            $this->appendAttribute('size', $this->size);
        }
        if(isset($this->src)) {
            $this->appendAttribute('src', $this->src);
        }
        if(isset($this->step)) {
            $this->appendAttribute('step', $this->step);
        }
        if(isset($this->type) && !is_null($this->type->value())) {
            $this->appendAttribute('type', $this->type->value());
        }
        if(isset($this->value)) {
            $this->appendAttribute('value', $this->value);
        }
        if(isset($this->width)) {
            $this->appendAttribute('width', $this->width);
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return \phphtml5\Enums\Accept
     */
    function getAccept() {
        return $this->accept;
    }

    /**
     * 
     * @return string
     */
    function getAlt() {
        return $this->alt;
    }

    /**
     * 
     * @return bool
     */
    function getAutocomplete() {
        return $this->autocomplete;
    }

    /**
     * 
     * @return bool
     */
    function getAutofocus() {
        return $this->autofocus;
    }

    /**
     * 
     * @return bool
     */
    function getChecked() {
        return $this->checked;
    }

    /**
     * 
     * @return bool
     */
    function getDisabled() {
        return $this->disabled;
    }

    /**
     * 
     * @return string
     */
    function getForm() {
        return $this->form;
    }

    /**
     * 
     * @return string
     */
    function getFormaction() {
        return $this->formaction;
    }

    /**
     * 
     * @return \phphtml5\Enums\EncType
     */
    function getFormenctype() {
        return $this->formenctype;
    }

    /**
     * 
     * @return \phphtml5\Enums\Method
     */
    function getFormmethod() {
        return $this->formmethod;
    }

    /**
     * 
     * @return bool
     */
    function getFormnovalidate() {
        return $this->formnovalidate;
    }

    /**
     * 
     * @return \phphtml5\Enums\Target
     */
    function getFormtarget() {
        return $this->formtarget;
    }

    /**
     * 
     * @return int
     */
    function getHeight() {
        return $this->height;
    }

    /**
     * 
     * @return string
     */
    function getList() {
        return $this->list;
    }

    /**
     * 
     * @return int|string
     */
    function getMax() {
        return $this->max;
    }

    /**
     * 
     * @return int
     */
    function getMaxlength() {
        return $this->maxlength;
    }

    /**
     * 
     * @return int|string
     */
    function getMin() {
        return $this->min;
    }

    /**
     * 
     * @return bool
     */
    function getMultiple() {
        return $this->multiple;
    }

    /**
     * 
     * @return string
     */
    function getName() {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    function getPattern() {
        return $this->pattern;
    }

    /**
     * 
     * @return string
     */
    function getPlaceholder() {
        return $this->placeholder;
    }

    /**
     * 
     * @return bool
     */
    function getReadonly() {
        return $this->readonly;
    }

    /**
     * 
     * @return bool
     */
    function getRequired() {
        return $this->required;
    }

    /**
     * 
     * @return int
     */
    function getSize() {
        return $this->size;
    }

    /**
     * 
     * @return string
     */
    function getSrc() {
        return $this->src;
    }

    /**
     * 
     * @return int
     */
    function getStep() {
        return $this->step;
    }

    /**
     * 
     * @return Type
     */
    function getType() {
        return $this->type;
    }

    /**
     * 
     * @return mixed
     */
    function getValue() {
        return $this->value;
    }

    /**
     * 
     * @return int
     */
    function getWidth() {
        return $this->width;
    }

    /**
     * 
     * @param \phphtml5\Enums\Accept $accept
     */
    function setAccept(\phphtml5\Enums\Accept $accept = null) {
        $this->accept = $accept;
    }

    /**
     * 
     * @param string $alt
     */
    function setAlt($alt) {
        if (is_null($alt)) {
            $this->alt = null;
        } elseif (is_string($alt)) {
            $this->alt = $alt;
        } else {
            throw new Exception('$alt isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param bool $autocomplete
     * @throws Exception
     */
    function setAutocomplete($autocomplete) {
        if (is_null($autocomplete)) {
            $this->autocomplete = null;
        } elseif (is_bool($autocomplete)) {
            $this->autocomplete = $autocomplete;
        } else {
            throw new Exception('$autocomplete isn\t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $autofocus
     * @throws Exception
     */
    function setAutofocus($autofocus) {
        if (is_null($autofocus)) {
            $this->autofocus = null;
        } elseif (is_bool($autofocus)) {
            $this->autofocus = $autofocus;
        } else {
            throw new Exception('$autocomplete isn\t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $checked
     * @throws Exception
     */
    function setChecked($checked) {
        if (is_null($checked)) {
            $this->checked = null;
        } elseif (is_bool($checked)) {
            $this->checked = $checked;
        } else {
            throw new Exception('$checked isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $disabled
     * @throws Exception
     */
    function setDisabled($disabled) {
        if (is_null($disabled)) {
            $this->disabled = null;
        } elseif (is_bool($disabled)) {
            $this->disabled = $disabled;
        } else {
            throw new Exception('$disabled isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $form
     * @throws Exception
     */
    function setForm($form) {
        if (is_null($form)) {
            $this->form = null;
        } elseif (is_string($form)) {
            $this->form = $form;
        } else {
            throw new Exception('$form isn\' a valid string!');
        }
    }

    /**
     * 
     * @param string $formaction
     * @throws Exception
     */
    function setFormaction($formaction) {
        if (is_null($formaction)) {
            $this->formaction = null;
        } elseif (is_string($formaction)) {
            $this->formaction = $formaction;
        } else {
            throw new Exception('$formaction isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param \phphtml5\Enums\EncType $formenctype
     */
    function setFormenctype(\phphtml5\Enums\EncType $formenctype = null) {
        $this->formenctype = $formenctype;
    }

    /**
     * 
     * @param \phphtml5\Enums\Method $formmethod
     */
    function setFormmethod(\phphtml5\Enums\Method $formmethod = null) {
        $this->formmethod = $formmethod;
    }

    function setFormnovalidate($formnovalidate) {
        if (is_null($formnovalidate)) {
            $this->formnovalidate = null;
        } elseif (is_bool($formnovalidate)) {
            $this->formnovalidate = $formnovalidate;
        } else {
            throw new Exception('$formnovalidate isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param \phphtml5\Enums\Target $formtarget
     */
    function setFormtarget(\phphtml5\Enums\Target $formtarget = null) {
        $this->formtarget = $formtarget;
    }

    /**
     * 
     * @param int $height
     * @throws Exception
     */
    function setHeight($height) {
        if (is_null($height)) {
            $this->height = null;
        } elseif (is_int($height)) {
            $this->height = $height;
        } else {
            throw new Exception('$height isn\'t a valid int!');
        }
    }

    /**
     * 
     * @param string $list
     * @throws Exception
     */
    function setList($list) {
        if (is_null($list)) {
            $this->list = null;
        } elseif (is_string($list)) {
            $this->list = $list;
        } else {
            throw new Exception('$list isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param int|string $max
     * @throws Exception
     */
    function setMax($max) {
        if (is_null($max)) {
            $this->max = null;
        } elseif (is_int($max) || preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $max)) {
            $this->max = $max;
        } else {
            throw new Exception('$max isn\'t a valid int|string!');
        }
    }

    /**
     * 
     * @param int $maxlength
     * @throws Exception
     */
    function setMaxlength($maxlength) {
        if (is_null($maxlength)) {
            $this->maxlength = null;
        } elseif (is_int($maxlength)) {
            $this->maxlength = $maxlength;
        } else {
            throw new Exception('$maxlength isn\'t a valid int!');
        }
    }

    /**
     * 
     * @param int|string $min
     * @throws Exception
     */
    function setMin($min) {
        if (is_null($min)) {
            $this->min = null;
        } elseif (is_int($min) || preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $min)) {
            $this->min = $min;
        } else {
            throw new Exception('$min isn\'t a valid int|string!');
        }
    }

    /**
     * 
     * @param bool $multiple
     * @throws Exception
     */
    function setMultiple($multiple) {
        if (is_null($multiple)) {
            $this->multiple = null;
        } elseif (is_bool($multiple)) {
            $this->multiple = $multiple;
        } else {
            throw new Exception('$multiple isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $name
     * @throws Exception
     */
    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif (is_string($name)) {
            $this->name = $name;
        } else {
            throw new Exception('$name isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param string $pattern
     * @throws Exception
     */
    function setPattern($pattern) {
        if (is_null($pattern)) {
            $this->pattern = null;
        } elseif (is_string($pattern)) {
            $this->pattern = $pattern;
        } else {
            throw new Exception('$pattern isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param string $placeholder
     * @throws Exception
     */
    function setPlaceholder($placeholder) {
        if (is_null($placeholder)) {
            $this->placeholder = null;
        } elseif (is_string($placeholder)) {
            $this->placeholder = $placeholder;
        } else {
            throw new Exception('$placeholder isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param bool $readonly
     * @throws Exception
     */
    function setReadonly($readonly) {
        if (is_null($readonly)) {
            $this->readonly = null;
        } elseif (is_bool($readonly)) {
            $this->readonly = $readonly;
        } else {
            throw new Exception('$readonly isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $required
     * @throws Exception
     */
    function setRequired($required) {
        if (is_null($required)) {
            $this->required = null;
        } elseif (is_bool($required)) {
            $this->required = $required;
        } else {
            throw new Exception('$required isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param int $size
     * @throws Exception
     */
    function setSize($size) {
        if (is_null($size)) {
            $this->size = null;
        } elseif (is_int($size)) {
            $this->size = $size;
        } else {
            throw new Exception('$size isn\'t a valjd int!');
        }
    }

    /**
     * 
     * @param string $src
     * @throws Exception
     */
    function setSrc($src) {
        if (is_null($src)) {
            $this->src = null;
        } elseif (is_string($src)) {
            $this->src = $src;
        } else {
            throw new Exception('$src isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param int $step
     * @throws Exception
     */
    function setStep($step) {
        if (is_null($step)) {
            $this->step = null;
        } elseif (is_int($step)) {
            $this->step = $step;
        } else {
            throw new Exception('$step isn\'t a valid int!');
        }
    }

    /**
     * 
     * @param \phphtml5\Enums\Type $type
     */
    function setType(\phphtml5\Enums\Type $type = null) {
        $this->type = $type;
    }

    /**
     * 
     * @param mixed $value
     */
    function setValue($value) {
        $this->value = $value;
    }

    /**
     * 
     * @param int $width
     * @throws Exception
     */
    function setWidth($width) {
        if (is_null($width)) {
            $this->width = null;
        } elseif (is_int($width)) {
            $this->width = $width;
        } else {
            throw new Exception('$width isn\'t a valid int!');
        }
    }
}