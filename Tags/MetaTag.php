<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 20:19:05
 */
namespace phphtml5\Tags;

class MetaTag extends TagWithGlobalAttributes {
     private $charset, $content, $httpEquiv, $name; // Attributes

     function __construct($charset = null, $content = null, $httpEquiv = null, $name = null) {
        parent::__construct('meta');
        $this->closetag = true;
        try {
           $this->setCharset($charset);
           $this->setContent($content);
           $this->setHttpEquiv($httpEquiv);
           $this->setName($name);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

     protected function sAttributes() {
        if (isset($this->charset)) {
            $this->appendAttribute('charset', $this->charset->value());
        }
        if (isset($this->content)) {
            $this->appendAttribute('content', $this->content);
        }
        if (isset($this->httpEquiv)) {
            $this->appendAttribute('http-equiv', $this->httpEquiv);
        }
        if (isset($this->name) && $this->name->value() != null) {
            $this->appendAttribute('name', $this->name->value());
        }
        return parent::sAttributes();
    }

    function getCharset() {
        return $this->charset;
    }

    function getContent() {
        return $this->content;
    }

    function getHttpEquiv() {
        return $this->httpEquiv;
    }

    function getName() {
        return $this->name;
    }

    function setCharset($charset) {
        if (is_null($charset)) {
            $this->charset = null;
        } elseif ($charset instanceof \phphtml5\Enums\Charset) {
            $this->charset = $charset;
        } else {
            throw new Exception('$charset is not a Enums/Charset!');
        }
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setHttpEquiv($httpEquiv) {
        $this->httpEquiv = $httpEquiv;
    }

    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif ($name instanceof \phphtml5\Enums\Name) {
            $this->name = $name;
        } else {
            throw new Exception('$name is not a Enums/Name!');
        }
    }
}