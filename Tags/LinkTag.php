<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 2-jul-2015
 * Time: 14:44:48
 */
namespace phphtml5\Tags;

class LinkTag extends TagWithGlobalAttributes {

    private $crossorigin, $href, $hreflang, $media, $rel, $sizes, $type; //Attributes

    function __construct(\phphtml5\Enums\Crossorigin $crossorigin = null, $href = null, \phphtml5\Enums\Language $hreflang = null, $media = null, \phphtml5\Enums\Rel $rel = null, $sizes = null, \phphtml5\Enums\MediaType $type = null) {
        parent::__construct('link');
        $this->oneLiner = TRUE;
        try {
            $this->setCrossorigin($crossorigin);
            $this->setHref($href);
            $this->setHreflang($hreflang);
            $this->setMedia($media);
//            if (is_null($rel)) {
//                $this->setRel(new Rel());
//            } else {
                $this->setRel($rel);
//            }
            $this->setSizes($sizes);
            $this->setType($type);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->crossorigin) && $this->crossorigin->value() != null) {
            $this->appendAttribute('crossorigin', $this->crossorigin->value());
        }
        if (isset($this->href)) {
            $this->appendAttribute('href', $this->href);
        }
        if (isset($this->hreflang) && $this->hreflang->value() != null) {
            $this->appendAttribute('hreflang', $this->hreflang->value());
        }
        if (isset($this->media)) {
            $this->appendAttribute('media', $this->media);
        }
        if (isset($this->rel) && $this->rel->value()) {
            $this->appendAttribute('rel', $this->rel->value());
        }
        if (isset($this->sizes)) {
            $this->appendAttribute('sizes', $this->sizes);
        }
        if (isset($this->type) && $this->type->value() != null) {
            $this->appendAttribute('type', $this->type->value());
        }
        return parent::sAttributes();
    }

    function getCrossorigin() {
        return $this->crossorigin;
    }

    function getHref() {
        return $this->href;
    }

    function getHreflang() {
        return $this->hreflang;
    }

    function getMedia() {
        return $this->media;
    }

    function getRel() {
        return $this->rel;
    }

    function getSizes() {
        return $this->sizes;
    }

    function getType() {
        return $this->type;
    }

    function setCrossorigin(\phphtml5\Enums\Crossorigin $crossorigin = null) {
        $this->crossorigin = $crossorigin;
    }

    function setHref($href) {
        if (is_null($href)) {
            $this->href = null;
        } elseif (is_string($href)) {
            $this->href = $href;
        } else {
            throw new Exception('$href isn\t a valid string!');
        }
    }

    function setHreflang(\phphtml5\Enums\Language $hreflang = null) {
        $this->hreflang = $hreflang;
    }

    function setMedia($media) {
        if (is_null($media)) {
            $this->media = null;
        } elseif (is_string($media)) {
            $this->media = $media;
        } else {
            throw new Exception('$media isn\'t a valid string!');
        }
    }

    function setRel(\phphtml5\Enums\Rel $rel = null) {
        $this->rel = $rel;
    }

    function setSizes($sizes) {
        if (is_null($sizes)) {
            $this->sizes = null;
        } elseif (is_string($sizes)) {
            $this->sizes = $sizes;
        } else {
            throw new \Exception('$sizes isn\'t a valid string!');
        }
    }

    function setType(\phphtml5\Enums\MediaType $type = null) {
        $this->type = $type;
    }
}