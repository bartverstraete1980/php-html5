<?php
namespace phphtml5\Tags;

/**
 * Description of TagWithGlobalAttributes
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class TagWithGlobalAttributes extends Tag {

    private $accesskey,$class,$contenteditable,$contextmenu,$data,$dir,
            $draggable,$dropzone,$hidden,$id,$lang,$spellcheck,$style,$tabindex,
            $title,$translate; // Attributes
    
    function __construct($tagname, Tag &$parent = null, $accesskey = null, $class = null,
            $contenteditable = null, $contextmenu = null, $data = null,
            $dir = null, $draggable = null, $dropzone = null, $hidden = null,
            $id = null, $lang = null, $spellcheck = null, $style = null,
            $tabindex = null, $title = null, $translate = null) {
        parent::__construct($tagname, $parent);
        try {
            $this->setAccesskey($accesskey);
            $this->setClass($class);
            $this->setContenteditable($contenteditable);
            $this->setContextmenu($contextmenu);
            $this->setData($data);
            $this->setDir($dir);
            $this->setDraggable($draggable);
            $this->setDropzone($dropzone);
            $this->setHidden($hidden);
            $this->setId($id);
            $this->setLang($lang);
            $this->setSpellcheck($spellcheck);
            $this->setStyle($style);
            $this->setTabindex($tabindex);
            $this->setTitle($title);
            $this->setTranslate($translate);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
    
    protected function sAttributes() {
        if (isset($this->accesskey)) {
            $this->appendAttribute('accesskey', $this->accesskey);
        }
        if (isset($this->class)) {
            $this->appendAttribute('class', $this->class);
        }
        if (isset($this->contenteditable)) {
            $this->appendAttribute('contenteditable', $this->contenteditable ? 'true' : 'false');
        }
        if (isset($this->contextmenu)) {
            $this->appendAttribute('contextmenu', $this->contextmenu);
        }
        if (isset($this->data)) {
            foreach ($this->data as $key => $value) {
                $this->appendAttribute('data-' .$key, $value);
            }
        }
        if (isset($this->dir)) {
            $this->appendAttribute('dir', $this->dir->value());
        }
        if (isset($this->draggable)) {
            $this->appendAttribute('draggable', $this->draggable);
        }
        if (isset($this->dropzone)) {
            $this->appendAttribute('dropzone', $this->dropzone);
        }
        if (isset($this->hidden)) {
            $this->appendAttribute('hidden');
        }
        if (isset($this->id)) {
            $this->appendAttribute('id', $this->id);
        }
        if (isset($this->lang)) {
            $this->appendAttribute('lang', $this->lang->value());
        }
        if (isset($this->spellcheck)) {
            $this->appendAttribute('spellcheck', $this->spellcheck);
        }
        if (isset($this->style)) {
            $this->appendAttribute('style', $this->style);
        }
        if (isset($this->tabindex)) {
            $this->appendAttribute('tabindex', $this->tabindex);
        }
        if (isset($this->title)) {
            $this->appendAttribute('title', $this->title);
        }
        if (isset($this->translate)) {
            $this->appendAttribute('translate', $this->translate);
        }
        return parent::sAttributes();
    }

    function getAccesskey() {
        return $this->accesskey;
    }

    function getClass() {
        return $this->class;
    }

    function getContenteditable() {
        return $this->contenteditable;
    }

    function getContextmenu() {
        return $this->contextmenu;
    }

    function getData() {
        return $this->data;
    }

    function getDir() {
        return $this->dir;
    }

    function getDraggable() {
        return $this->draggable;
    }

    function getDropzone() {
        return $this->dropzone;
    }

    function getHidden() {
        return $this->hidden;
    }

    function getId() {
        return $this->id;
    }

    function getLang() {
        return $this->lang;
    }

    function getSpellcheck() {
        return $this->spellcheck;
    }

    function getStyle() {
        return $this->style;
    }

    function getTabindex() {
        return $this->tabindex;
    }

    function getTitle() {
        return $this->title;
    }

    function getTranslate() {
        return $this->translate;
    }

    /**
     * @param string $accesskey
     */
    public function setAccesskey($accesskey){
        if (is_null($accesskey)) {
            $this->accesskey = null;
        } elseif (is_string($accesskey)) {
            if (strlen($accesskey) == 1) {
                $this->accesskey = $accesskey;
            } else {
              throw new Exception('The lenght of the string may only be "1" character!');
            };;
        } else {
            throw new Exception('$acceskey is not a string!');
        }
    }

    /**
     * @param string $class
     */
    public function setClass($class){
        if (is_null($class)) {
            $this->class = null;
        } elseif (is_string($class)) {
            $this->class = $class;
        } else {
            throw new Exception('$class is not a string!');
        }
    }

    /**
     * @param bool $contenteditable
     */
    public function setContenteditable($contenteditable){
        if (is_null($contenteditable)) {
            $this->contenteditable = null;
        } elseif (is_bool($contenteditable)) {
            $this->contenteditable = $contenteditable;
        } else {
            throw new Exception('$contenteditable is not a boolean!');
        }
    }

    /**
     * @param string $cont
     * 
     * 
     * 
     * 
     * 
     * 
     * extmenu
     */
    public function setContextmenu($contextmenu){
        if (is_null($contextmenu)) {
            $this->contextmenu = null;
        } elseif (is_string($contextmenu)) {
            $this->contextmenu = $contextmenu;
        } else {
            throw new Exception('$contextmenu is not a string!');
        }
    }

    /**
     * @param array $data
     */
    public function setData($data){
        if (is_null($data)) {
            $this->data = null;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                if (!is_string($key)) {
                    throw new Exception('$data has not all strings as keys!');
                }
            }
            $this->data = $data;
        } else {
            throw new Exception('$data is not a array!');
        }
    }

    /**
     * @param Dir $dir
     */
    public function setDir($dir){
        if (is_null($dir)) {
            $this->dir = null;
        } elseif ($dir instanceof Dir) {
                $this->dir = $dir;
        } else {
            throw new Exception('$dir is not a Dir enum!');
        }
    }

    /**
     * @param bool $dra---ggable
     */
    public function setDraggable($draggable){
        if (is_null($draggable)) {
            $this->draggable = null;
        } elseif (is_bool($draggable)) {
            $this->draggable = $draggable;
        } else {
            throw new Exception('$draggable is not a bool!');
        }
    }

    /**
     * @param DROPZONE* $dropzone
     */
    public function setDropzone($dropzone){
        if (is_null($dropzone)) {
            $this->dropzone = null;
        } elseif (is_string($dropzone)) {
            switch ($dropzone) {
                case TagWithGlobalAttributes::DROPZONECopy:
                case TagWithGlobalAttributes::DROPZONELink:
                case TagWithGlobalAttributes::DROPZONEMove:
                    $this->dropzone = $dropzone;
                    break;
                default:
                    throw new Exception('$dropzone is not one of the const');
            }
        }  else {
            throw new Exception('$dropzone is not a DROPBOX* const!');
        }
    }

    /**
     * @param bool $hidden
     */
    public function setHidden($hidden){
        if (is_null($hidden)) {
            $this->hidden = null;
        } elseif (is_bool($hidden)) {
            if ($hidden == true) {
                $this->hidden = true;
            } else {
                $this->hidden = null;
            }
        } else {
            throw new Exception('$hidden is not a bool!');
        }
    }

    /**
     * @param string $id
     */
    public function setId($id){
        if (is_null($id)) {
            $this->id = null;
        } elseif (is_string($id)) {
            $this->id = $id;
        } else {
            throw new Exception('$id is not a string!');
        }
    }

    /**
     * @param Language $lang
     */
    public function setLang($lang){
        if (is_null($lang)) {
            $this->lang = null;
        } elseif ($lang instanceof \phphtml5\Enums\Language) {
            $this->lang = $lang;
        } else {
            throw new Exception('$lang is not a Enums/Language!');
        }
    }

    /**
     * @param bool $spellcheck
     */
    public function setSpellcheck($spellcheck){
        if (is_null($spellcheck)) {
            $this->spellcheck = null;
        } elseif (is_bool($spellcheck)) {
            $this->spellcheck = $spellcheck;
        } else {
            throw new Exception('$spellcheck is not a bool!');
        }
    }

    /**
     * @param string $style
     */
    public function setStyle($style){
        if (is_null($style)) {
            $this->style = null;
        } elseif (is_string($style)) {
            $this->style = $style;
        } else {
            throw new Exception('$style is not a string!');
        }
    }

    /**
     * @param integer $tabindex
     */
    public function setTabindex($tabindex){
        if (is_null($tabindex)) {
            $this->tabindex = null;
        } elseif (is_int($tabindex)) {
            $this->tabindex = $tabindex;
        } else {
            throw new Exception('$tabindex is not a integer!');
        }
    }

    /**
     * @param string $title
     */
    public function setTitle($title){
        if (is_null($title)) {
            $this->title = null;
        } elseif (is_string($title)) {
            $this->title = $title;
        } else {
            throw new Exception('$title is not a string!');
        }
    }

    /**
     * @param bool $translate
     */
    public function setTranslate($translate){
        if (is_null($translate)) {
            $this->translate = null;
        } elseif (is_bool($translate)) {
            $this->translate = $translate;
        } else {
            throw new Exception('$translate is not a bool!');
        }
    }

}