<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 20-jun-2015
 * Time: 17:06:54
 */
namespace phphtml5\Tags;

class SpanTag extends TagWithGlobalAttributes {

    function __construct() {
        parent::__construct('span');
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}
