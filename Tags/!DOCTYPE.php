<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 6-jun-2015
 * Time: 15:22:03
 */
namespace phphtml5\Tags;

class DOCTYPE extends Tag {

    private $declaration;

    function __construct(\phphtml5\Enums\Declaration $declaration, Tag &$parent = null) {
        parent::__construct('!DOCTYPE', $parent);
        try {
            $this->closetag = true;
            $this->declaration = $declaration;
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        $this->appendAttribute($this->declaration->value());
        return parent::sAttributes();
    }
}