<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 5-jun-2015
 * Time: 16:14:00
 */
namespace phphtml5\Tags;

class Tag {
    private $tagname;
    private $elements = [], $attributes = [];
    private $parent = null;
    private $tabs = 0;
    private $element_count = 0;
    protected $oneLiner = false;
    protected $closetag = false;
    protected $notabs = false;
    protected $norets = false;
            
    function __construct($tagname, Tag &$parent = null) {
        try {
            $this->setTagName($tagname);
            if (isset($parent)) {
                $this->parent = $parent;
                $this->tabs = $parent->tabs +1;
            }
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
    public function __toString() {
        $f = $this->element_count == 0 && $this->closetag;
        if ($f) {
            return $this->sOpenCloseTag();
        } else {
            return sprintf("%s%s%s", $this->sOpenTag(), $this->sElements(),
                    $this->sCloseTag());
        }
    }

    /**
     * 
     * @param string $key
     * @param string $value
     * @throws Exception
     */
    public function appendAttribute($key, $value = null) {
        if (is_string($key) && is_string($value)) {
            $this->attributes[$key] = $value;
        } elseif (is_string($key) && is_null($value)) {
            $this->attributes[$key] = null;
        } else {
            throw new Exception('$key is no string and $value is no string or null!');
        }
    }
    
    /**
     * 
     * @param Tag|string $element
     * @throws Exception
     */
    public function appendElement($element) {
         if ($element instanceof Tag) {
            if(!isset($element->parent)) {
                $element->parent = &$this;
                $element->tabs = $element->parent->getTabs() +1;
            }
            $this->elements[] = $element;
            $this->element_count++;
        } elseif (is_string($element)) {
            $this->elements[] = $element;
            $this->element_count++;
            $this->oneLiner = $this->element_count == 1 ? true : false;
        } elseif (is_null($element)) {
            //Nothing to append
        } else {
            throw new Exception('$element is not a Tag or string!');
        }
        if ($this->element_count > 1) {
            $this->oneLiner = false;
        }
        return $element;
    }
    
    /**
     * 
     * @return string
     */
    protected function sAttributes() {
        $ret = '';
        foreach ($this->attributes as $key => $value) {
            $ret .= sprintf(' %s%s', $key, $value ? sprintf('="%s"', $value) : '');
        }
        return $ret;
    }
    
    protected function sElements() {
        $ret = '';
        foreach ($this->elements as $element) {
            if ($element instanceof Tag) {
                $ret .= $element;
            } elseif (is_string($element)) {
                if (!$this->oneLiner) {
                    $element = str_replace("\n", "<br>\n" .str_repeat("\t", $this->getTabs() +1), trim($element));
                }
                $ret .= sprintf("%s%s", !$this->oneLiner ? "\n" .str_repeat("\t", $this->getTabs() +1) : '', $element);
          }
        }
        return $ret;
    }
    /**
     * 
     * @return string
     */
    protected function sCloseTag() {
        return sprintf("%s</%s>", !$this->oneLiner ? "\n" .str_repeat("\t",
                $this->getTabs()) : '', $this->getTagname());
    }

    /**
     * 
     * @return string
     */
    protected function sOpenCloseTag() {
        $f = ($this->parent == null && $this->element_count == 0);
        return sprintf("%s%s<%s%s>", $f ? '' : "\n" , str_repeat("\t",
                $this->getTabs()), $this->getTagname(), $this->sAttributes());
    }

    
    /**
     * 
     * @return string
     */
    protected function sOpenTag() {
        $f = ($this->parent || $this->element_count > 0 );
//        echo $f?'true':'false';
        return sprintf("%s%s<%s%s>", $this->norets ? '':$f ? "\n":'', $this->notabs ? '' : str_repeat("\t",
                $this->getTabs()), $this->getTagname(), $this->sAttributes());
    }

    function getElements() {
        return $this->elements;
    }

    /**
     * 
     * @return int
     */
    function getTabs() {
        return $this->tabs;
    }

    /**
     * 
     * @return string
     */
    function getTagname() {
        return $this->tagname;
    }
    function setParent(Tag &$parent) {
        $this->parent = $parent;
    }

        /**
     * 
     * @param string $tagname
     * @throws Exception
     */
    function setTagName($tagname) {
        if (is_string($tagname)) {
            if (!preg_match("/^[!]?[a-zA-Z]\w*/", $tagname)) {
                throw new Exception('$tagname is not a valid string!');
            }
            $this->tagname = $tagname;
        }  else {
            throw new Exception('$tagname is not a string!');
        }
   }
   
}
