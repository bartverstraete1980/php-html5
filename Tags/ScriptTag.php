<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 30-jun-2015
 * Time: 11:07:50
 */
namespace phphtml5\Tags;

class ScriptTag extends TagWithGlobalAttributes {

    private $async, $charset, $defer, $src, $type;

    function __construct($async = null, \phphtml5\Enums\Charset $charset = null, $defer = null, $src = null, \phphtml5\Enums\MediaType $type = null) {
        parent::__construct('script');
        $this->oneLiner = true;
        try {
            $this->setAsync($async);
            $this->setCharset($charset);
            $this->setDefer($defer);
            $this->setSrc($src);
            $this->setType($type);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if (isset($this->async) && $this->async == true) {
            $this->appendAttribute('async');
        }
        if (isset($this->charset) && $this->charset->value() != null) {
            $this->appendAttribute('charset', $this->charset->value());
        }
        if (isset($this->defer) && $this->defer == true) {
            $this->appendAttribute('defer');
        }
        if (isset($this->src)) {
            $this->appendAttribute('src', $this->src);
        }
        if (isset($this->type) && $this->type->value() != null) {
            $this->appendAttribute('type', $this->type->value());
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return boolean
     */
    function getAsync() {
        if (is_bool($this->async)) {
            return $this->async;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return \phphtml5\Enums\Charset
     */
    function getCharset() {
        if ($this->charset instanceof \phphtml5\Enums\Charset) {
            return $this->charset;
        } else {
            return new \phphtml5\Enums\Charset();
        }
    }

    /**
     * 
     * @return boolean
     */
    function getDefer() {
        if (is_bool($this->defer)) {
            return $this->defer;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return string
     */
    function getSrc() {
        if (is_string($this->src)) {
            return $this->src;
        } else {
            return '';
        }
    }

    /**
     * 
     * @return \phphtml5\Enums\MediaType
     */
    function getType() {
        if ($this->type instanceof \phphtml5\Enums\MediaType) {
            return $this->type;
        } else {
            return new \phphtml5\Enums\MediaType();
        }
    }

    /**
     * 
     * @param boolean $async
     * @throws Exception
     */
    function setAsync($async) {
        if (is_null($async)) {
            $this->async = null;
        } elseif (is_bool($async)) {
            $this->async = $async;
        } else {
            throw new Exception('$async isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param Charset $charset
     */
    function setCharset(\phphtml5\Enums\Charset $charset = null) {
        $this->charset = $charset;
    }

    /**
     * 
     * @param boolean $defer
     * @throws Exception
     */
    function setDefer($defer) {
        if (is_null($defer)) {
            $this->defer = null;
        } elseif (is_bool($defer)) {
            $this->defer = $defer;
        } else {
            throw new Exception('$defer isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $src
     * @throws Exception
     */
    function setSrc($src) {
        if (is_null($src)) {
            $this->src = null;
        } elseif (is_string($src)) {
            $this->src = $src;
        } else {
            throw new Exception('$src isn\'t a valid string!');
        }
    }

    function setType(\phphtml5\Enums\MediaType $type = null) {
        $this->type = $type;
    }

}
