<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 12:49:09
 */
namespace phphtml5\Tags;

class DivTag extends TagWithGlobalAttributes {

    function __construct(Tag &$parent = null) {
        parent::__construct('div', $parent);
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}