<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 13-jun+--2015
 * Time: 19:29:08
 */
namespace phphtml5\Tags;

use Exception;

class OptionTag extends TagWithGlobalAttributes {
    private $disabled, $label, $selected, $value; //Attributes

    function __construct(Tag &$parent, $disabled = null, $label = null,
            $selected = null, $value = null) {
        parent::__construct('option', $parent);
        try {
            $this->setDisabled($disabled);
            $this->setLabel($label);
            $this->setSelected($selected);
            $this->setValue($value);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if(isset($this->disabled) && $this->disabled) {
            $this->appendAttribute('disabled');
        }
        if(isset($this->label)) {
            $this->appendAttribute('label', $this->label);
        }
        if(isset($this->selected) && $this->selected) {
            $this->appendAttribute('selected');
        }
        if(isset($this->value)) {
            $this->appendAttribute('value', $this->value);
        }
        return parent::sAttributes();
    }

    /**
     * 
     * @return bool
     */
    function getDisabled() {
        return $this->disabled;
    }

    /**
     * 
     * @return string
     */
    function getLabel() {
        return $this->label;
    }

    /**
     * 
     * @return bool
     */
    function getSelected() {
        return $this->selected;
    }

    /**
     * 
     * @return string
     */
    function getValue() {
        return $this->value;
    }

    /**
     * 
     * @param bool $disabled
     * @throws Exception
     */
    function setDisabled($disabled) {
        if (is_null($disabled)) {
            $this->disabled = null;
        } elseif (is_bool($disabled)) {
            $this->disabled = $disabled;
        } else {
            throw new Exception('$disabled isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $label
     * @throws Exception
     */
    function setLabel($label) {
        if (is_null($label)) {
            $this->label = null;
        } elseif (is_string($label)) {
            $this->label = $label;
        } else {
            throw new Exception('$label isn\t a valid string');
        }
    }

    /**
     * 
     * @param bool $selected
     * @throws Exception
     */
    function setSelected($selected) {
        if (is_null($selected)) {
            $this->selected = null;
        } elseif (is_bool($selected)) {
            $this->selected = $selected;
        } else {
            throw new Exception('$selcted isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $value
     * @throws Exception
     */
    function setValue($value) {
        if (is_null($value)) {
            $this->value = null;
        } elseif (is_string($value)) {
            $this->value = $value;
        } else {
            throw new Exception('$value isn\t a valid string');
        }
    }
}