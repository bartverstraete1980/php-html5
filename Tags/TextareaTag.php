<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 20-jun-2015
 * Time: 11:15:04
 */
namespace phphtml5\Tags;

class TextareaTag extends TagWithGlobalAttributes {
    private $autofocus, $cols, $disabled, $form, $maxlength, $name, $placeholder,
            $readonly, $required, $rows, $wrap; //Attributes
    
    function __construct($autofocus = null, $cols = null, $disabled = null, 
            $form = null, $maxlength = null, $name = null, $placeholder = null,
            $readonly = null, $required = null, $rows = null, Wrap $wrap = null) {
        parent::__construct('textarea');
        try {
            $this->setAutofocus($autofocus);
            $this->setCols($cols);
            $this->setDisabled($disabled);
            $this->setForm($form);
            $this->setMaxlength($maxlength);
            $this->setName($name);
            $this->setPlaceholder($placeholder);
            $this->setReadonly($readonly);
            $this->setRequired($required);
            $this->setRows($rows);
            $this->setWrap($wrap);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
    protected function sAttributes() {
        if(isset($this->autofocus) && $this->autofocus) {
            $this->appendAttribute('autofocus');
        }
        if(isset($this->cols)) {
            $this->appendAttribute('cols', $this->cols);
        }
        if(isset($this->disabled) && $this->disabled) {
            $this->appendAttribute('disabled');
        }
        if(isset($this->form)) {
            $this->appendAttribute('form', $this->form);
        }
        if(isset($this->maxlength)) {
            $this->appendAttribute('maxlength', $this->maxlength);
        }
        if(isset($this->name)) {
            $this->appendAttribute('name', $this->name);
        }
        if(isset($this->placeholder)) {
            $this->appendAttribute('placeholder', $this->placeholder);
        }
        if(isset($this->readonly) && $this->readonly) {
            $this->appendAttribute('readonly');
        }
        if(isset($this->required) && $this->required) {
            $this->appendAttribute('required');
        }
        if(isset($this->rows)) {
            $this->appendAttribute('rows', $this->rows);
        }
        if(isset($this->wrap) && $this->wrap->value()) {
            $this->appendAttribute('wrap', $this->wrap->value());
        }
        return parent::sAttributes();
    }
 
    /**
     * 
     * @return bool
     */
    function getAutofocus() {
        return $this->autofocus;
    }

    /**
     * 
     * @return int
     */
    function getCols() {
        return $this->cols;
    }

    /**
     * 
     * @return bool
     */
    function getDisabled() {
        return $this->disabled;
    }

    /**
     * 
     * @return string
     */
    function getForm() {
        return $this->form;
    }

    /**
     * 
     * @return int
     */
    function getMaxlength() {
        return $this->maxlength;
    }

    /**
     * 
     * @return string
     */
    function getName() {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    function getPlaceholder() {
        return $this->placeholder;
    }

    /**
     * 
     * @return bool
     */
    function getReadonly() {
        return $this->readonly;
    }

    /**
     * 
     * @return bool
     */
    function getRequired() {
        return $this->required;
    }

    /**
     * 
     * @return iny
     */
    function getRows() {
        return $this->rows;
    }

    /**
     * 
     * @return Wrap
     */
    function getWrap() {
        return $this->wrap;
    }

    /**
     * 
     * @param bool $autofocus
     * @throws Exception
     */
    function setAutofocus($autofocus) {
        if (is_null($autofocus)) {
            $this->autofocus = null;
        } elseif (is_bool($autofocus)) {
            $this->autofocus = $autofocus;
        } else {
            throw new Exception('$autofocus isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param int $cols
     * @throws Exception
     */
    function setCols($cols) {
        if (is_null($cols)) {
            $this->cols = null;
        } elseif (is_int($cols)) {
            $this->cols = $cols;
        } else {
            throw new Exception('$cols isn\'t a valid int!');
        }
    }

    /**
     * 
     * @param bool $disabled
     * @throws Exception
     */
    function setDisabled($disabled) {
        if (is_null($disabled)) {
            $this->disabled = null;
        } elseif (is_bool($disabled)) {
            $this->disabled = $disabled;
        } else {
            throw new Exception('$disabled isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param string $form
     * @throws Exception
     */
    function setForm($form) {
        if (is_null($form)) {
            $this->form = null;
        } elseif (is_string($form)) {
            $this->form = $form;
        } else {
            throw new Exception('$form isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param int $maxlength
     * @throws Exception
     */
    function setMaxlength($maxlength) {
        if (is_null($maxlength)) {
            $this->maxlength = null;
        } elseif (is_int($maxlength)) {
            $this->maxlength = $maxlength;
        } else {
            throw new Exception('$maxlength isn\' a valid int!');
        }
    }

    /**
     * 
     * @param string $name
     * @throws Exception
     */
    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif (is_string($name)) {
            $this->name = $name;
        } else {
            throw new Exception('$name isn\'t a valid string!');
        }
    }

    /**
     * 
     * @param string $placeholder
     * @throws Exception
     */
    function setPlaceholder($placeholder) {
        if (is_null($placeholder)) {
            $this->placeholder = null;
        } elseif (is_string($placeholder)) {
            $this->placeholder = $placeholder;
        } else {
            throw new Exception('$placeholder isn\t a valid string!');
        }
    }

    /**
     * 
     * @param bool $readonly
     * @throws Exception
     */
    function setReadonly($readonly) {
        if (is_null($readonly)) {
            $this->readonly = null;
        } elseif (is_bool($readonly)) {
            $this->readonly = $readonly;
        } else {
            throw new Exception('$rreadonly isn\'t a valid bool!');
        }
    }

    /**
     * 
     * @param bool $required
     * @throws Exception
     */
    function setRequired($required) {
        if (is_null($required)) {
            $this->required = null;
        } elseif (is_bool($required)) {
            $this->required = $required;
        } else {
            throw new Exception('$required isn\t a valid bool!');
        }
    }

    /**
     * 
     * @param int $rows
     * @throws Exception
     */
    function setRows($rows) {
        if (is_null($rows)) {
            $this->rows = null;
        } elseif (is_int($rows)) {
            $this->rows = $rows;
        } else {
            throw new Exception('$rows isn\' a valid int!');
        }
    }

    /**
     * 
     * @param Wrap $wrap
     */
    function setWrap(Wrap $wrap = null) {
        $this->wrap = $wrap;
    }
}