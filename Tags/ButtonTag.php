<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 8-jul-2015
 * Time: 13:44:10
 */
namespace phphtml5\Tags;

class ButtonTag extends TagWithGlobalAttributes {

    private $autofocus, $disabled, $form, $formaction, $formenctype, $formmethod, $formnovalidate, $formtarget, $name, $type, $value;

    function __construct(Tag &$parent = null, $autofocus = null, $disabled = null, $form = null, $formaction = null, \phphtml5\Enums\EncType $formenctype = null, \phphtml5\Enums\Method $formmethod = null, $formnovalidate = null, \phphtml5\Enums\Target $formtarget = null, $name = null, \phphtml5\Enums\ButtonType $type = null, $value = null) {
        parent::__construct('button', $parent);
        try {
            $this->setAutofocus($autofocus);
            $this->setDisabled($disabled);
            $this->setForm($form);
            $this->setFormaction($formaction);
            $this->setFormenctype($formenctype);
            $this->setFormmethod($formmethod);
            $this->setFormnovalidate($formnovalidate);
            $this->setFormtarget($formtarget);
            $this->setName($name);
            $this->setType($type);
            $this->setValue($value);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    protected function sAttributes() {
        if(isset($this->autofocus) && $this->autofocus == true) {
            $this->appendAttribute('autofocus');
        }
        if(isset($this->disabled) && $this->disabled == true) {
            $this->appendAttribute('disabled');
        }
        if(isset($this->form)) {
            $this->appendAttribute('form', $this->form);
        }
        if(isset($this->formaction)) {
            $this->appendAttribute('formaction', $this->formaction);
        }
        if(isset($this->formenctype) && $this->formenctype->value() != null) {
            $this->appendAttribute('formenctype', $this->formenctype->value());
        }
        if(isset($this->formmethod) && $this->formmethod->value() != null) {
            $this->appendAttribute('formmethod', $this->formmethod->value());
        }
        if(isset($this->formnovalidate) && $this->formnovalidate == true) {
            $this->appendAttribute('formnovalidate');
        }
        if(isset($this->formtarget) && $this->formtarget->value() != null) {
            $this->appendAttribute('formtarget', $this->formtarget->value());
        }
        if(isset($this->name)) {
            $this->appendAttribute('name', $this->name);
        }
        if(isset($this->type) && $this->type->value() != null) {
            $this->appendAttribute('type', $this->type->value());
        }
        if(isset($this->value)) {
            $this->appendAttribute('value', $this->value);
        }
        return parent::sAttributes();
    }

    function getAutofocus() {
        return $this->autofocus;
    }

    function getDisabled() {
        return $this->disabled;
    }

    function getForm() {
        return $this->form;
    }

    function getFormaction() {
        return $this->formaction;
    }

    function getFormenctype() {
        return $this->formenctype;
    }

    function getFormmethod() {
        return $this->formmethod;
    }

    function getFormnovalidate() {
        return $this->formnovalidate;
    }

    function getFormtarget() {
        return $this->formtarget;
    }

    function getName() {
        return $this->name;
    }

    function getType() {
        return $this->type;
    }

    function getValue() {
        return $this->value;
    }
/*
        if (is_null($)) {
            $this-> = null;
        } elseif (is_bool($)) {
            $this->= $;
        } else {
            throw new Exception('$ isn\'t a valid bool!');
        }

        if (is_null($)) {
            $this-> = null;
        } elseif (is_string($)) {
            $this-> = $;
        } else {
            throw new Exception('$ isn\'t a valid string!');
        }
 */
    function setAutofocus($autofocus) {
        if (is_null($autofocus)) {
            $this->autofocus = null;
        } elseif (is_bool($autofocus)) {
            $this->autofocus = $autofocus;
        } else {
            throw new Exception('$autofocus isn\'t a valid bool!');
        }
    }

    function setDisabled($disabled) {
        if (is_null($disabled)) {
            $this->disabled = null;
        } elseif (is_bool($disabled)) {
            $this->disabled= $disabled;
        } else {
            throw new Exception('$disabled isn\'t a valid bool!');
        }
    }

    function setForm($form) {
        if (is_null($form)) {
            $this->form = null;
        } elseif (is_string($form)) {
            $this->form = $form;
        } else {
            throw new Exception('$form isn\'t a valid string!');
        }
    }

    function setFormaction($formaction) {
        if (is_null($formaction)) {
            $this->formaction = null;
        } elseif (is_string($formaction)) {
            $this->formaction = $formaction;
        } else {
            throw new Exception('$formaction isn\'t a valid string!');
        }
    }

    function setFormenctype(\phphtml5\Enums\EncType $formenctype = null) {
        if (is_null($formenctype)) {
            $this->formenctype = new \phphtml5\Enums\EncType();
        } else {
            $this->formenctype = $formenctype;
        }
    }

    function setFormmethod(\phphtml5\Enums\Method $formmethod = null) {
        if (is_null($formmethod)) {
            $this->formmethod = new \phphtml5\Enums\Method();
        } else {
            $this->formmethod = $formmethod;
        }
    }

    function setFormnovalidate($formnovalidate) {
        if (is_null($formnovalidate)) {
            $this->formnovalidate = null;
        } elseif (is_bool($formnovalidate)) {
            $this->formnovalidate = $formnovalidate;
        } else {
            throw new Exception('$formnovalidate isn\'t a valid bool!');
        }
    }

    function setFormtarget(\phphtml5\Enums\Target $formtarget = null) {
        if (is_null($formtarget)) {
            $this->formtarget = new \phphtml5\Enums\Target();
        } else {
            $this->formtarget = $formtarget;
        }
    }

    function setName($name) {
        if (is_null($name)) {
            $this->name = null;
        } elseif (is_string($name)) {
            $this->name = $name;
        } else {
            throw new Exception('$name isn\'t a valid string!');
        }
    }

    function setType(\phphtml5\Enums\ButtonType $type = null) {
        if (is_null($type)) {
            $this->type = new \phphtml5\Enums\ButtonType();
        } else {
            $this->type = $type;
        }
    }

    function setValue($value) {
        if (is_null($value)) {
            $this->value = null;
        } elseif (is_string($value)) {
            $this->value = $value;
        } else {
            throw new Exception('$value isn\'t a valid string!');
        }
    }
}