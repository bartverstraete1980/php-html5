<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 23:33:07
 */
namespace phphtml5\Basic;

class LabelTextarea extends \phphtml5\Tags\LabelTag {

    function __construct($form_id, $label_text, $name, \phphtml5\Tags\Tag &$parent,
            $autofocus = false) {
        parent::__construct($parent, null, $form_id);
        try {
            $this->setStyle('display: flex; flex-direction: column;');
            $this->setClass('label_textarea');
            $this->appendElement(new Span($label_text))->setStyle('order: 1;');;
            $textarea = $this->appendElement(new \phphtml5\Tags\TextareaTag($autofocus, null,
                    null, $form_id, null, $name, 'Geef uw commentaar', null,
                    true, null, null));
            $textarea->setStyle('order: 2; width: 20em;');
//            $textarea->setAutofocus(true);
//            $textarea->setName($name);
//            $textarea->setRequired(true);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}