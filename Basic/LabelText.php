<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 7-jul-2015
 * Time: 10:51:07
 */
namespace phphtml5\Basic;

class LabelText extends \phphtml5\Tags\LabelTag {

    function __construct($form_id, $label_text, $name, \phphtml5\Tags\Tag &$parent, $autofocus = false) {
        parent::__construct($parent, null, $form_id);
        try {
            $this->setClass('label_text');
            $this->appendElement(new Span($label_text));
            $this->appendElement(new \phphtml5\Tags\InputTag(null, null, null, $autofocus, null, null, $form_id, null, null, null, null, null, null, null, null, null, null, null, $name, null, null, null, null, null, null, null, new \phphtml5\Enums\Type(\phphtml5\Enums\Type::TEXT), null, null));
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}