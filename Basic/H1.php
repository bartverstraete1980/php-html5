<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 11-jun-2015
 * Time: 17:43:16
 */
namespace phphtml5\Basic;

class H1 extends \phphtml5\Tags\HxTag {

    function __construct($text = 'H2') {
        parent::__construct(1);
        try {
            $this->appendElement($text);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}