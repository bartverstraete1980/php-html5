<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 5-jul-2015
 * Time: 21:02:29
 */
namespace phphtml5\Basic;

class NavigationBox extends \phphtml5\Tags\DivTag {

    function __construct(\phphtml5\Tags\Tag &$parent, $data = []) {
        parent::__construct($parent);
        try {
            $this->setId('navigation_box');
//            $ul = $this->appendElement(new UlTag($this));
//            foreach ($data as $value) {
//                $li = new LiTag($ul);
//                $ul->appendElement($li);
//                $a = new ATag($li, null, $value[0]);
//                $li->appendElement($a);
//                $button = new ButtonTag($a, null, null, null, null, null, null, null, null, null, new ButtonType(ButtonType::BUTTON), null);
//                $button->appendElement($value[1]);
//                $button->oneLiner = true;
//                
//                $a->appendElement($button);
//            }
            foreach ($data as $uldata) {
                $ul = $this->appendElement(new \phphtml5\Tags\UlTag($this));
                foreach ($uldata as $lidata) {
                    $li = $ul->appendElement(new \phphtml5\Tags\LiTag($ul));
                    if (isset($lidata)) {
                        $item = $li->appendElement($lidata);
                    }
                }
            }
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}