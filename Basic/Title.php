<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 9-jun-2015
 * Time: 18:07:56
 */
namespace phphtml5\Basic;

class Title extends \phphtml5\Tags\TitleTag {

    function __construct($title) {
        parent::__construct();
        try {
            $this->appendElement($title);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}