<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 20-jun-2015
 * Time: 17:19:05
 */
namespace phphtml5\Basic;

class Span extends \phphtml5\Tags\SpanTag {

    function __construct($text, $oneLiner = true) {
        parent::__construct();
        try {
            $this->appendElement($text);
            $this->oneLiner = $oneLiner;
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}