<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 17:22:24
 */
namespace phphtml5\Basic;

class Li extends \phphtml5\Tags\LiTag {

    function __construct($item, \phphtml5\Tags\Tag &$parent = null) {
        parent::__construct($parent);
        try {
            $this->appendElement($item);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}