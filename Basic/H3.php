<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 12:21:38
 */
namespace phphtml5\Basic;

class H3 extends \phphtml5\Tags\HxTag {

    function __construct($text = 'H3') {
        parent::__construct(3);
        try {
            $this->appendElement($text);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}