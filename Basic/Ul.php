<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 17:17:50
 */
namespace phphtml5\Basic;

class Ul extends \phphtml5\Tags\UlTag {

    function __construct($data = [], \phphtml5\Tags\Tag &$parent = null) {
        parent::__construct($parent);
        try {
            foreach ($data as $value) {
                $li = new \phphtml5\Tags\LiTag($this);
                $this->appendElement($li);
                $li->appendElement($value);
            }
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}