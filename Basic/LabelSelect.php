<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 13-jun-2015
& * Time: 21:39:45
 */
namespace phphtml5\Basic;

class LabelSelect extends \phphtml5\Tags\LabelTag {

    function __construct($data = [], $form_id, $label_text, $name, \phphtml5\Tags\Tag &$parent,
            $selected = null, $autofocus = false) {
        parent::__construct($parent, $name, $form_id);
        try {
//            $this->setId($id); //setId checks $id otherwise it throws an Exception
            $this->setClass('label_select');
            if (is_string($label_text)) {
                $this->appendElement($label_text);
            }
            $this->appendElement($select = new \phphtml5\Tags\SelectTag($autofocus,
                    NULL,
                    $form_id,
                    NULL,
                    $name,
                    TRUE));
            $select->setId($name);
            foreach ($data as $key => $value) {
                $select->appendElement($option = new \phphtml5\Tags\OptionTag(NULL, 
                        NULL,
                        $selected == $key,
                        $key));
                $option->appendElement($value);
            }
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}