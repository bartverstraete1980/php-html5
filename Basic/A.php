<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 16:14:44
 */
namespace phphtml5\Basic;

class A extends \phphtml5\Tags\ATag {

    function __construct($href, $hreftext) {
        parent::__construct(null, $href);
        try {
            $this->appendElement($hreftext);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}