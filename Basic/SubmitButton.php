<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 9:58:40
 */
namespace phphtml5\Basic;

class SubmitButton extends \phphtml5\Tags\InputTag {

    function __construct($label = 'SUBMIT', $id = 'submit_button') {
        parent::__construct(null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null,
                new \phphtml5\Enums\Type(\phphtml5\Enums\Type::SUBMIT), $label,
                null);
        try {
            $this->setId($id);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}