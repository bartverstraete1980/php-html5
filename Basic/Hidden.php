<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 24-jun-2015
 * Time: 10:02:54
 */
namespace phphtml5\Basic;

class Hidden extends \phphtml5\Tags\InputTag {

    function __construct($name, $value, $form) {
        parent::__construct(null, null, null, null, null, null, $form, null, null, null, null, null, null, null, null, null, null, null, $name, null, null, null, null, null, null, null, new \phphtml5\Enums\Type(\phphtml5\Enums\Type::HIDDEN), $value, null);
        try {
            
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

}
