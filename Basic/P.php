<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 17:47:07
 */
namespace phphtml5\Basic;

class P extends \phphtml5\Tags\PTag {

    function __construct($text =  'P', $oneLiner = false) {
        parent::__construct();
        try {
            $this->appendElement($text);
            $this->oneLiner = $oneLiner;
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}