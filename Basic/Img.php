<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 28-jul-2015
 * Time: 9:51:22
 */
namespace phphtml5\Basic;

class Img extends \phphtml5\Tags\ImgTag {

    private $file;

    function __construct($file, $id = null) {
        parent::__construct();
        try {
            $this->setFile($file);
            $this->setId($id);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        if (exif_imagetype($file)) {
            $this->file = $file;
            $this->setSrc(URL . $file);
        } else {
            throw new Exception('$file isn\'t a valid image!');
        }
    }
}