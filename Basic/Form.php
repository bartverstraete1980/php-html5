<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 10:39:37
 */
namespace phphtml5\Basic;

class Form extends \phphtml5\Tags\FormTag {

    function __construct($name, $action = null, \phphtml5\Enums\Method $method = null) {
        parent::__construct(null, $action, null, null, $method, $name, null, null);
        try {
            $this->setId("form_$name");
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}