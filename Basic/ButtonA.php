<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 8-jul-2015
 * Time: 16:42:25
 */
namespace phphtml5\Basic;

class ButtonA extends \phphtml5\Tags\ATag {
    private $button;

    function __construct($href, $label_text, \phphtml5\Tags\Tag &$parent = null) {
        parent::__construct($parent, null, $href, null, null, null, null, null);
        try {
            $this->oneLiner = true;
            $this->button = new \phphtml5\Tags\ButtonTag($this, null, null, null, null, null, null, null, null, null, new \phphtml5\Enums\ButtonType(\phphtml5\Enums\ButtonType::BUTTON), null);
            $this->appendElement($this->button);
            $this->button->notabs = true;
            $this->button->norets = true;
            $this->button->appendElement($label_text);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }

    function setDisabled($param) {
        $this->button->setDisabled($param);
        $this->setHref(null);
    }
}