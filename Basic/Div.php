<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 19-jun-2015
 * Time: 13:21:26
 */
namespace phphtml5\Basic;

class Div extends \phphtml5\Tags\DivTag {

    function __construct($id = 'Div', \phphtml5\Tags\Tag &$parent = NULL) {
        parent::__construct($parent);
        try {
            $this->setId($id);
        } catch (Exception $exc) {
            handleException($exc);
        }
    }
}