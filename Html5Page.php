<?php
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 6-jun-2015
 * Time: 22:09:07
 */
namespace phphtml5;

use Exception;
use phphtml5\Basic\Title;
use phphtml5\Enums\Charset;
use phphtml5\Enums\Declaration;
use phphtml5\Enums\Language;
use phphtml5\Enums\MediaType;
use phphtml5\Enums\Name;
use phphtml5\Enums\Rel;
use phphtml5\Tags\BodyTag;
use phphtml5\Tags\DOCTYPE;
use phphtml5\Tags\HeadTag;
use phphtml5\Tags\HtmlTag;
use phphtml5\Tags\LinkTag;
use phphtml5\Tags\MetaTag;

include_once 'Tags/!DOCTYPE.php';

class Html5Page {

    private $doctype;
    private $html;
    protected $body, $head;
    private $exception;

    function __construct(Language $lang, Charset $charset, $title = 'Html5Page',
            $description = 'HTML5Page description', $fav ='favicon.ico') {
        try {
            $this->doctype = new DOCTYPE(new Declaration(Declaration::HTML5));
            $this->html = new HtmlTag();
            $this->html->setLang($lang);
            $this->head = new HeadTag();
            $this->html->appendElement($this->head);
            $this->head->appendElement(new MetaTag($charset));
            $this->head->appendElement(new Title($title));
            $this->head->appendElement(new MetaTag(null, $description, null, $description ? new Name(Name::DESCRIPTION) : null));
            $this->head->appendElement(new LinkTag(null,  $fav,null, null,
                    new Rel(Rel::ICON), null,
                    new MediaType(MediaType::IMAGEXICON)));
            $this->body = new BodyTag();
            $this->html->appendElement($this->body);
        } catch (Exception $exc) {
            $GLOBALS['handleException']($exc);
        }
    }

    public function __toString() {
        return $this->doctype . $this->html;
    }

    function auto_copyright($owner, $year = 'auto') {
        if (intval($year) == 'auto') {
            $year = date('Y');
        }
        if (intval($year) == date('Y')) {
            return sprintf('&copy; %d %s', intval($year), $owner);
        } elseif (intval($year) < date('Y')) {
            return sprintf('&copy; %d - %d %s', $year, date('Y'), $owner);
        } elseif (intval($year) > date('Y')) {
            return sprintf('&copy; %d %s', date('Y'), $owner);
        }
    }

    /**
     * 
     * @return BodyTag
     */
    function getBody() {
        return $this->body;
    }

    /**
     * 
     * @return HeadTag
     */
    function getHead() {
        return $this->head;
    }
}